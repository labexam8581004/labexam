const express =  require('express')
const app = express()
const cors = require('cors')
app.use(cors('*'))
const mysql = require('mysql2')
const connection = mysql.createConnection({
    host: '172.17.0.1',
    user: 'root',
    port: 7000,
    password: 'manager',
    database: 'test'
  });
app.use(express.json());

app.get('/',(req,res)=>{
    connection.query(
        'SELECT * FROM Movie_Tb',
        function(err, results, fields) {
         if(err!=null){
            res.send(err);
         }
         else{
            res.send(results);
         }
        }
      );
})
// movie_id int,movie_title varchar(20),movie_release_date varchar(20),movie_time varchar(20),director_name varchar(20))
app.post('/',(req,res)=>{
    connection.query(
        `insert into Movie_Tb values(${req.body.movie_id},'${req.body.movie_title}',
        '${req.body.movie_release_date}','${req.body.movie_time}','${req.body.director_name}')`,
        function(err, results, fields) {
         if(err!=null){
            res.send(err);
         }
         else{
            res.send(results);
         }
        }
      );
})

app.delete('/:no',(req,res)=>{
    connection.query(
        `delete from Movie_Tb where movie_id = ${req.params.no}`,
        function(err, results, fields) {
         if(err!=null){
            res.send(err);
         }
         else{
            res.send(results);
         }
        }
      );
})
app.listen(3000,'0.0.0.0',()=>{
    console.log("server started at 3000")
})